import Interfaces.WD412Interface;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import commons.CsvJDBC;
import commons.RESTAPI;
import commons.Utils;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WiremockServer {

    public static void main(String args[]) {

        WireMockServer wireMockServer1 = new WireMockServer(new WireMockConfiguration().port(8081));
                WireMockServer wireMockServer2 = new WireMockServer(new WireMockConfiguration().port(8082));/*.extensions(new ResponseDefinitionTransformer() {

            @Override
            public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource, Parameters parameters) {

                Utils utils = new Utils();

                String[] response = utils.getResponse(request.getUrl(), request.getBodyAsString());
                return new ResponseDefinitionBuilder().like(responseDefinition)
                        .withBody(response[0].getBytes())
                        .withHeader("Content-Type", "application/json")
                        .withStatus(Integer.parseInt(response[1]))
                        .build();
            }

            public String getName() {
                return "request body returning request transformer";

            }
        }));
*/
        wireMockServer1.start();
        wireMockServer2.start();

        RESTAPI restapi = new RESTAPI();
        Utils utils = new Utils();

        // ################################################
        //      Interface end system MOCK
        // ################################################
        //WD412STL Interface
       /* WD412Interface wd412Interface = new WD412Interface();
        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock412"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/octet-stream")
                        .withStatus(200)));

        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock229"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json;charset=UTF-8")
                        .withBody(utils.saveBodyFile("workday/239.json"))));

        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock230"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/octet-stream")
                        .withBodyFile("workday/230.json")));
        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock231"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/octet-stream")
                        .withBodyFile("workday/231.json")));

        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock231"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/octet-stream")
                        .withBodyFile("workday/230231.json")));
        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock415a"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/octet-stream")
                        .withBodyFile("workday/415a.json")));
        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/mock415b"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/octet-stream")
                        .withBodyFile("workday/415b.json")));
        // ################################################
        //      Mulesoft stub
        // ################################################
        //WD412STlInterface
        wireMockServer.stubFor(post(urlPathEqualTo("/hr/experience/onboarding-api/human_resources/starters"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                ));

        //=======================================
        //WD229Service Letter
        //=======================================
        wireMockServer.stubFor(post(urlPathMatching("/hr/experience/serviceletter-api/rewards/workers/[0-9]+"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                ));
        //.withBody(restapi.postResponse("http://localhost:8083/hr/experience/mock229","{{request.body}}")).withTransformers("response-template")));

        //=======================================
        //WD230231Thunderbird
        //=======================================
        wireMockServer.stubFor(post(urlPathMatching("/hr/experience/rewardsletter-api/rewards/workers/[0-9]+"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json;charset=UTF-8")
                ));
    }*/

        wireMockServer1.stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withStatus(200)));

        wireMockServer2.stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withStatus(200)));

        wireMockServer1.stubFor(post(urlEqualTo("/t3m/DoRiskJSON"))
                .willReturn(aResponse()
                        //.withHeader("Content-Type","application/json")
                        .withHeader("Server", "one")
                        .withBodyFile("workday/239.json")
                        .withStatus(200)));
        wireMockServer2.stubFor(post(urlEqualTo("/t3m/DoRiskJSON"))
                .willReturn(aResponse()
                        .withBodyFile("workday/239.json")
                        .withHeader("Content-Type","application/json")
                        .withHeader("Server", "two")
                        .withStatus(200)));

    }
}
