package commons;

import java.sql.*;
import java.util.*;

public class CsvJDBC {
    private static Connection connection = null;
    private Statement statement;
    private static final String jdbcDriver = "org.relique.jdbc.csv.CsvDriver";

    public CsvJDBC() {
        try {
            Class.forName(jdbcDriver).newInstance();
            connection = DriverManager.getConnection("jdbc:relique:csv:C:\\Users\\vthaduri\\Projects\\skylight-automation\\SkylightCommons\\target\\classes\\TestData\\testData.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CsvJDBC(String path) {
        try {
            Class.forName(jdbcDriver).newInstance();
            path = path.replace("/", "\\").substring(1, path.length());
            connection = DriverManager.getConnection("jdbc:relique:csv:" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getCSVResponse(String table, String request) {

        String response = "";
        String query = "select * from " + table + " where request = " + "'"+ request +"'";
        try {
            statement = connection.createStatement();
            ResultSet results = statement.executeQuery(query);
            ResultSetMetaData meta = results.getMetaData();

            while (results.next()) {

                for (int i = 0; i < meta.getColumnCount(); i++) {
                    response = results.getString("response");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}