package commons;

import com.fasterxml.jackson.databind.JsonNode;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONStub {

    private static final Configuration configuration = Configuration.builder()
            .jsonProvider(new JacksonJsonNodeJsonProvider())
            .mappingProvider(new JacksonMappingProvider())
            .build();

    public String createJSONStub(String json, List<Map<String, String>> scenarios)
    {
        for (int i = 0; i < scenarios.size(); i++) {
            String jpath = scenarios.get(i).get("NodeJpath");
            String value = scenarios.get(i).get("NodeValue");
            JsonNode updatedJson = JsonPath.using(configuration).parse(json).set(jpath, value).json();
            json = updatedJson.toString();
        }
        return json;
    }

    public String getJsonNodeValue(String json, String jpath)
    {
        JsonNode getJsonValue = JsonPath.using(configuration).parse(json).read(jpath);
        return getJsonValue.asText();
    }

    public Map<String,String> getWD412Interface(String json)
    {
        Map<String,String> map = new HashMap<String, String>();
        map.put("ID",getJsonNodeValue(json,"$.ID"));
        map.put("bp",getJsonNodeValue(json,"$.bp"));
        return map;
    }
}
