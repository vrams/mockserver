package commons;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils
{
    public String saveBodyFile(String mockFileName)
    {
        String fileToSave = "";
        URL location = this.getClass().getProtectionDomain().getCodeSource().getLocation();
        String result = location.getPath().replace("classes","test-classes")+ "__files/" + mockFileName;
        try {
            fileToSave = convertInputStreamToString(new FileInputStream(result));
            FileUtils.writeStringToFile(new File("mulesoft_mock_" + getBaseFilename(mockFileName)), fileToSave, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileToSave;
    }

    public static void saveRequestBody(String requestBody, String interfaces)
    {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        String fileName = "mulesoft_request_" + interfaces + "_" + timeStamp + ".txt";
        String filePath = "C:/Temp/"+ interfaces;
        try {
            FileUtils.writeStringToFile(new File( filePath + "/"+ fileName), requestBody, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getBaseFilename(String filePath)
    {
        return FilenameUtils.getName(filePath);
    }


    public static String convertInputStreamToString(InputStream inputStream)
    {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public String[] getResponse(String url,String request)
    {
        String response[] = new String[2];
        CsvJDBC csvJDBC;
        if(url.equalsIgnoreCase("/hr/experience/onboarding-api/human_resources/starters"))
        {
           if (request.contains("transfer"))
           {
               Utils.saveRequestBody(request, "WD415aDocHold");
               csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD415aDocHold");
               response[0] = csvJDBC.getCSVResponse("WD415aDocHold", request);
               response[1] = "200";
           }
            else if (request.contains("termination"))
            {
                Utils.saveRequestBody(request, "WD415bDocHold");
                csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD415bDocHold");
                response[0] = csvJDBC.getCSVResponse("WD415bDocHold", request);
                response[1] = "200";
            }
           else {
               Utils.saveRequestBody(request, "WD412STLInterface");
               csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD412STLInterface");
               response[0] = csvJDBC.getCSVResponse("WD412STLInterface", request);
               response[1] = "200";

               if (request.contains("Bad Request"))
               {
                   response[1] = "400";
               }
           }
           if (request.contains("Bad Request") && request.contains("WD415aDocHold"))
           {
               Utils.saveRequestBody(request, "WD415aDocHold");
               csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD415aDocHold");
               response[0] = csvJDBC.getCSVResponse("WD415aDocHold", request);
               response[1] = "400";
           }
           else if (request.contains("Bad Request") && request.contains("WD415bDocHold"))
           {
               Utils.saveRequestBody(request, "WD415bDocHold");
               csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD415bDocHold");
               response[0] = csvJDBC.getCSVResponse("WD415bDocHold", request);
               response[1] = "400";
           }
        }
        else if(url.contains("/hr/experience/rewardsletter-api/rewards/workers"))
        {
            if (request.contains("Includes internal mobility"))
            {
                Utils.saveRequestBody(request, "WD230Thunderhead");
                csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD230Thunderhead");
                response[0] = csvJDBC.getCSVResponse("WD230Thunderhead", request);
                response[1] = "200";
            }
            else if (request.contains("Change of Commission Plan"))
            {
                Utils.saveRequestBody(request, "WD231Thunderhead");
                csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD231Thunderhead");
                response[0] = csvJDBC.getCSVResponse("WD231Thunderhead", request);
                response[1] = "200";
            }

            else if (request.contains("Bad Request") && request.contains("WD230Thunderhead"))
            {
                Utils.saveRequestBody(request, "WD230Thunderhead");
                csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD230Thunderhead");
                response[0] = csvJDBC.getCSVResponse("WD230Thunderhead", request);
                response[1] = "400";
            }

            else if (request.contains("Bad Request") && request.contains("WD231Thunderhead"))
            {
                Utils.saveRequestBody(request, "WD231Thunderhead");
                csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD231Thunderhead");
                response[0] = csvJDBC.getCSVResponse("WD231Thunderhead", request);
                response[1] = "400";
            }
        }
        else if(url.contains("/hr/experience/serviceletter-api/rewards/workers/"))
        {
            Utils.saveRequestBody(request, "WD229ServiceLetter");
            csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "WD229ServiceLetter");
            response[0] = csvJDBC.getCSVResponse("WD229ServiceLetter", request);
            response[1] = "200";

            if (request.contains("Bad Request"))
            {
                response[1] = "400";
            }
        }

        return response;
    }
}
