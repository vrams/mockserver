package Interfaces;

import commons.CsvJDBC;
import commons.Utils;

public class WD412Interface {

    public String returnResponse(String requestBody)
    {

        String response = "";
        CsvJDBC csvJDBC;


        try {
            csvJDBC = new CsvJDBC(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()  + "WD412STLInterface");
            response = csvJDBC.getCSVResponse("WD412Scenarios",requestBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils utils = new Utils();
        utils.saveRequestBody(requestBody, "WD412STLInterface");
        return  response;
    }
}
