@IntegrationTest
Feature: Spring boot template - Integration Tests
  I want to test my Rest Controller
  By running Integration Tests
  As part of building my application

  @GetRequest
  Scenario: Get Request with success
    Given I get request /to-read
    Then I verify the status code is 200

  @GetRequest @ContentType
  Scenario: Get Request with success with Content-Type
    Given I set Content-Type as application/json
    When I get request /to-read
    Then I verify the status code is 200

  @GetRequest @InvalidURL
  Scenario: Get Request with Invalid URL
    Given I get request /to-read-app
    Then I verify the status code is 404

  @GetRequest @ValidateJSON @VerifyJSON
  Scenario: Validate error json response
    Given I set Content-Type as application/json
    When I get request /error
    Then I verify the status code is 500
    Then I verify each field in /error response
    Then I validate the error json schema
