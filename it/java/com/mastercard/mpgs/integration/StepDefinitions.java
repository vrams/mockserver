package com.mastercard.mpgs.integration;

import com.mastercard.mpgs.config.SpringBootWebApplication;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

public class StepDefinitions {

    public static boolean applicationRun = false;
    public StepDefinitions()
    {
        if (!applicationRun) {
            SpringApplication.run(SpringBootWebApplication.class);
            applicationRun = true;
        }

    }

    ConfigurableApplicationContext context;
    private RestAssured restAssured;
    private Header header;
    private RequestSpecification requestSpecification;
    protected Response response;


    @Before
    public void Setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
        requestSpecification = RestAssured.given();
    }

    @Given("^I call the rest controller$")
    public void i_call_the_rest_controller() throws Throwable {
        response = requestSpecification.get("/to-read");
    }

    @Then("^I verify the status$")
    public void i_verify_the_status() throws Throwable {
        response.then().assertThat().statusCode(200);
        System.out.println("Response: *** " + response.getBody().toString());
    }

    @Given("^I get request (.*)$")
    public void i_get_request_to_read(String url) throws Throwable {
        response = requestSpecification.get(url);
    }

    @Then("^I verify the status code is (\\d+)$")
    public void i_verify_the_status_code_is(int status) throws Throwable {
        response.then().assertThat().statusCode(status);
    }

    @Given("^I set (Content-Type) as (.*)$")
    public void i_set_Content_Type_as_application_json(String key, String value) throws Throwable {
        requestSpecification.header(key, value);
    }

    @Then("^I verify each field in (.*) response$")
    public void i_verify_each_field_in_error_response(String url) throws Throwable {
        String datePattern = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}(\\+|\\-)\\d{4}$";
        if (url.equalsIgnoreCase("/error"))
        {
            response.then().assertThat()
                    .body("timestamp", matchesPattern(datePattern))
                    .body("status", equalTo(999))
                    .body("error", containsString("None"))
                    .body("message", containsString("No message available"));


        }

    }

    @Then("^I validate the (.*) json schema$")
    public void i_validate_the_error_json_schema(String fileName) throws Throwable {
        fileName = fileName + ".json";
        response.then().assertThat().body(matchesJsonSchemaInClasspath("json" + File.separator + fileName));
    }

}
