package com.mastercard.mpgs.integration;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class RESTAPI {
    private static final Logger logger = LoggerFactory.getLogger(RESTAPI.class);
    /*Get response from API */
    public String getResponse(String url)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        logger.info("JSON Response: " + responseEntity.toString());
        return responseEntity.toString();
    }
    /*POST response to API */
    public String postResponse(String url,String requestJson)
    {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);
        return restTemplate.postForObject(url, entity, String.class);
    }

    /*POST response to API */
    public String postResponse(String url)
    {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        String requestJson = "{\"success_flag\":\"Y\",\"code\":\"200\",\"message\":\"Starter successfully submitted for processing\"}";
        logger.info("Hitting Mock 229" + url);
        HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);
        return restTemplate.postForObject(url, entity, String.class);
    }

    public String postHTTPSRequest(String url,String requestJson)
    {
        //SSL Context
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        //Initiate REST Template
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //Send the Request and get the response.
        HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
        return response.getBody();
    }

    public String muleSoftRequest(String requestBody)
    {

        System.out.println(requestBody);
        return requestBody;
    }
}
