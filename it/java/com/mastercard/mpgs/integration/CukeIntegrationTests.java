package com.mastercard.mpgs.integration;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/it/resources/features",
        plugin = {
                "junit:target/test-reports.xml",
                "json:target/cucumber.json"
        }
        //,tags = "@test"
)
public class CukeIntegrationTests {

}

